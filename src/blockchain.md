Self-Sovereign Digital Identity is built upon [public-key cryptography](crypto.md) and [blockchain](https://en.wikipedia.org/wiki/Blockchain) technologies, making it possible to *independently* publish and verify the identity of individuals, organizations, and their devices, without the involvement of a trusted intermediary (traditionally, a [certificate authority](https://en.wikipedia.org/wiki/Certificate_authority)).  This can provide effective resistance to censorship, manipulation of data, and mass surveillance by eliminating the necessity of trusted third parties.  The use of a public blockchain is necessary to make *distributed* identity possible.


Although Lorena has its own public blockchain based on Parity Substrate ([lorena-substrate](https://gitlab.com/caelum-tech/lorena/lorena-substrate)), it also works with other public blockchains.

Systems like Lorena which implement [Decentralized IDs](https://www.w3.org/TR/did-core/) use a blockchain to allow the establishment of a unique identifier which is visible to anyone, without intermediaries or permissions required for reading or writing.  This allows the Subject (or Holder) to associate their identity with a public key so that an Issuer or Verifier can authenticate their messages and encrypt data for them.

Because cryptographic hash functions become less effective over time as brute-force attacks become more economically feasible (due to increasingly cheap computational power) or as new vulnerabilities are discovered in the underlying cryptographic algorithms, the use of a blockchain allows these hashes to be nested inside other hashes (which can be upgraded to new algorithms over time).  Thus, a blockchain allows us to use a cryptographic hash function to ensure the integrity of data despite the inevitability of hash collision attacks.

For Lorena to use a blockchain for storing an identity, that blockchain must be capable of the following:

* Allow new users to write new entries to the blockchain, signed with keys that they generate and hold themselves.
* Allow new users to somehow acquire permission to write to the blockchain, either through a faucet or a proxy service.
* Establish a unique address which can be queried by anyone, but can only be updated by the owner.  This becomes the DID.
* Write a string of data which corresponds to a URI (or hash thereof) associated with that DID.  This becomes the URI for the DID Document, usually the hash for a document stored in [IPFS](https://ipfs.io/).  The DID owner must be able to update this value with a "most recent" value which can be queried.

