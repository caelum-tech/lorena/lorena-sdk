# Introduction

**Lorena** provides Self-Sovereign Digital Identity, a method that lets people control how and where their personal information is used while accessing services online.  It is based on the [W3C](https://www.w3.org/) [Decentralized Identifiers](https://w3c-ccg.github.io/did-spec) and [Verifiable Credentials](https://www.w3.org/TR/vc-data-model/) standards.

Self-Sovereign Digital Identity is built upon [public-key cryptography](crypto.md) and [blockchain](blockchain.md) technologies, making it possible to *independently* publish and verify the identity of individuals, organizations, and their devices, without the involvement of a trusted intermediary (traditionally, a [certificate authority](https://en.wikipedia.org/wiki/Certificate_authority)).  This can provide effective resistance to censorship, manipulation of data, and mass surveillance by eliminating the necessity of trusted third parties.  The use of a [public blockchain](blockchain.md) is necessary to make *distributed* identity possible.

Instead of trusting certificate authorities and centralized identity registries to hold the keys, **Self-Sovereign Digital Identity allows users to keep their own private keys *and their data* private**, and build a set of attestations or proofs about their identity, to which they can control access.  They can even avoid sharing the actual data, and instead use [zero-knowledge proofs](https://en.wikipedia.org/wiki/Zero-knowledge_proof) to demonstrate that certain facts about them are true (e.g. proof of age, residency, nationality) **without sharing their personally identifiable information**.

This is a benefit to individuals who are increasingly concerned about their privacy, and to organizations which are held liable for careless treatment of data with huge penalties (especially under the [GDPR](https://en.wikipedia.org/wiki/General_Data_Protection_Regulation)).  It allows for a new world of peer-to-peer issuance and verification of credentials without intermediaries, and for more complex credentials to be built upon combinations of other credentials (e.g. discounts for resident full-time students under 25) without having to share all of the sensitive data with potentially untrustworthy parties.

## Roles
Each of the following [roles](https://www.w3.org/TR/vc-use-cases/) denotes an entity (person or organization) which holds a unique Decentralized Identifier (DID) stored in a blockchain along with a [public key](https://en.wikipedia.org/wiki/Public-key_cryptography), which together can be used to secure communication and verify continuity of identity between all parties.

* The **Subject** is the entity to which the information pertains. The **Holder** is the entity which is holding that information.  For the purposes of Lorena, Subject and Holder are treated as the same.

* The **Issuer** is the entity which validates (either manually or automatically) a piece of information about the Subject, and provides a digital signature (signed with the public key associated with the Issuer's DID) that attests to this validation.

* The **Verifier** is the entity which wants to know something about the Subject.  The Verifier needs to authenticate information about the Subject, and it trusts the Issuer's judgement on that information.

The entities playing these roles all have equal standing: each one has a DID, a (secret) private key and its associated public key which is published with the DID on the blockchain, and each can hold Verifiable Credentials (VCs).  The roles can shift, and can be a peer-to-peer relationship or hierarchical as appropriate to needs.

## Activities
Each activity is backed by a library which implements the necessary functionality on various platforms.

Each entity establishes a DID by creating a private-public key pair and creating an identity containing that public key on a public blockchain.  *See [Creating a DID](creating-a-did.md).*

The Subject can create new credentials and receive Verified Credentials from Issuers. *See [Requesting issuance of a VC](requesting-issuance-of-a-vc.md).*

The Verifier can request verified credentials from the Subject, and check that the Issuer signed them. *See [Requesting and verifying a VC](requesting-and-verifying-a-vc.md).*

## Components

### Services
![Component Communications](../public/component-communications.png "Component Communications")

The [Lorena Identity Playground](https://gitlab.com/caelum-tech/Lorena/lorena-playground) is a [React](https://reactjs.org/) [single-page application](https://en.wikipedia.org/wiki/Single-page_application) which implements a prototype [identity container](identity-container.md) which allows a **Subject (Holder)** to create a DID, create credentials and ask an *Issuer* to issue a VC, and send a VC to a *Verifier*.

[Hoverfloat](https://gitlab.com/caelum-tech/Lorena/lorena-demo-hoverfloat) is a [demonstration website](https://hoverfloat.com) for a fictional transportation service. It is written in [React](https://reactjs.org/) as a [single-page application](https://en.wikipedia.org/wiki/Single-page_application) with no server back-end. It acts as a **Verifier** which takes VCs sent by a *Subject* and makes sure they were issued by a trusted *Issuer*.

[Lorena Matrix Daemon](https://gitlab.com/caelum-tech/Lorena/lorena-matrix-daemon) is a Node JS back-end service which acts as an **Issuer** of VCs upon request by **Subjects**.  It implements an email verification service and coordinates communication between anonymous guest accounts through Matrix.

[Lorena Substrate](https://gitlab.com/caelum-tech/Lorena/lorena-substrate) is a [Substrate](https://www.parity.io/substrate/)-based blockchain for publishing DIDs and their associated public keys.

[Matrix](https://matrix.org/) is decentralized communication service used for secure communicate between the different entities and services.

### Applications

The [Lorena Personal Data Store (PDS)](identity-container.md#pds) is an [identity container](identity-container.md) used by people to manage their identities and verifiable credentials.

### Libraries
The [Lorena Matrix Client](https://gitlab.com/caelum-tech/Lorena/lorena-matrix-client) is a Javascript library which provides communication between Entities via Matrix for the services above.  It is used in Hoverfloat and the Playground.  It has two primary use cases: for Verifiers (e.g. sites which need identity verification), or for Subjects / Holders (e.g. cryptographic wallet providers).

[Lorena Matrix Helpers](https://gitlab.com/caelum-tech/Lorena/lorena-matrix-helpers) is a Javascript library which contains a set of Matrix-specific helper functions for the creation of accounts.

[caelum-diddoc](https://gitlab.com/caelum-tech/caelum-diddoc) is a Rust library (with Javascript bindings) for the creation and manipulation of DID Documents.

[caelum-vcdm](https://gitlab.com/caelum-tech/caelum-vcdm) is a Rust library (with [Javascript bindings](https://www.npmjs.com/package/@caelum-tech/caelum-vcdm)) for the manipulation of W3C [Verifiable Credential Data Model](https://www.w3.org/TR/verifiable-claims-data-model/) Verifiable Credentials (VCs) and Verifiable Presentations.

## Licensing
All components are all available under various open-source licenses.
