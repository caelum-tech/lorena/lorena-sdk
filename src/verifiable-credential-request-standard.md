# Verifiable Credential Request Standard

The Verifiable Credential Request (VCR) standard allows a Verifier to tell a Holder its own Decentralized ID (DID), which types of Verifiable Credentials (VCs) it requires, the transport by which those credentials can be transmitted.

When a [Verifier requests a Verifiable Credential](requesting-and-verifying-a-vc.md), it provides a URL to the Holder/Subject. This can be provided with a hyperlink or by scanning a QR code.

## Format

The format of the request is a URL

`BaseURL + base64( { body } )`

which looks like

`https://lorena.caelumlabs.com?request=u9f2iAFHEe34oiwSDReIemAEoe3jseIDjJsLDKFJEeBB23wo`

## Base URL
The Base URL is a standard URL which points to an Identity Container, such as the [Lorena Playground](http://lorena.caelumlabs.com), plus a standard parameter:

`http://lorena.caelumlabs.com?request=`

The Base URL is used to invoke the identity container if necessary, whether through a web interface or through a mobile application which opens that URL (through [Android App Links](https://developer.android.com/training/app-links) or [iOS Universal Links](https://developer.apple.com/ios/universal-links/)).

## Body
The core of the specification is a JSON object which has three parts:
```javascript
{
  transport: {
    type: 'matrix-guest',
    address: '@guest101:matrix.org'
  },
  type: uri,
  id: 'did:lorena:23849072389472389742389478'
 }
 ```

### Transport
The transport is the method by which the Subject/Holder can transmit the requested VCs to the Verifier.  One of the supported protocols is [Matrix](matrix.md) which can be `matrix-user` for registered, confirmed Matrix accounts, or `matrix-guest` for Matrix guest accounts.

### Type
The type is a [Uniform Resource Identifier (URI)](https://en.wikipedia.org/wiki/Uniform_Resource_Identifier). It specifies the VC(s) being requested

It may be a [URN](https://en.wikipedia.org/wiki/Uniform_Resource_Name) which identifies a specific VC type:
* `urn:lorena:vc-email-recipient`

Further (pretend) examples:
* `urn:eu:es:sepe:certificado-prestaciones` (Spanish unemployment certificate)
* `urn:eu:es:seg-social:numero` (Spanish Social Security number)
* `urn:eu:fr:gouv:diplomatie:vc-passeport` (French passport)
* `urn:eu:europa:europarl:mep` (EU Parliament members)
* `urn:mx:gob:sedena:cartilla` (Mexican military service)
* `urn:us:gov:ssa:ssn` (US Social Security Number)

... or it may be a [URL](https://en.wikipedia.org/wiki/Uniform_Resource_Locator) which resolves to a document which lists a more complex set of one or more VCs.
* `https://hoverfloat.com/requested-credentials`
... which may contain further definition of the VCs requested, and may change over time.

### DID
Specifies the DID of the Verifier.  This is used by the Subject/Holder to authenticate the identity of the Verifier, and to set up an encryption channel between the two parties (after retrieving the public key for that DID from the blockchain).
