Requesting and verifying a Verifiable Credential

The Verifier can request verified credentials from the Subject, and check that the Issuer signed them.

In Lorena this is implemented in the [Hoverfloat demo website](https://gitlab.com/caelum-tech/Lorena/lorena-dema-hoverfloat) (the **Verifier**) and the [Lorena Identity Playground](https://gitlab.com/caelum-tech/Lorena/lorena-playground) (the **Subject**), and finally the [Lorena Matrix Daemon](https://gitlab.com/caelum-tech/Lorena/lorena-matrix-daemon) (the **Issuer**) which issued the signed credentials.

![Requesting and verifying a VC sequence diagram](../public/verifier-subject-flow.png "Requesting and verifying a VC sequence diagram")

## Comprehensive flow
The story begins with a Subject who wishes to access a service online.  The Verifier wishes to verify something about the Subject, so it creates a [Verifiable Credential Request](verifiable-credential-request-standard.md) and presents it in a QR code.

Since the Verifier and Subject do not have publicly addressable endpoints the components use Matrix to communicate with each other.  Because the Verifier and Subject are each single-page applications operating without verified Matrix accounts, we use Matrix Guest account (having limited capabilities to avoid spam), which join an accessible public room in Matrix to request an invitation to a private room where they can exchange information securely.

In this case, the story continues with the Subject taking a picture of the QR code shown on the Subject website.  The QR code contains a URL for `https://lorena.caelumlabs.com/?request=XXXXXX` where `XXXXXX` contains a urlencoded representation of a JSON object with specifies the credential type requested, the DID of the Subject, and the transport mechanism through which the Subject can be contacted. *(see [Verifiable Credential Request Standard](verifiable-credential-request-standard.md))*

The Subject uses a guest account to join the public Matrix room and asks to be connected to the guest ID of the Verifier.  A [daemon](https://gitlab.com/caelum-tech/Lorena/lorena-matrix-daemon) sees this request and invites both guests to a new room, which the daemon then leaves.

Once the Verifier and Subject are in a private room together, they mutually verify their identities and agree on the terms of use (TOU) for the data that will be shared. This is in the form of a smart contract signed (and stored) by both parties.

After the secure channel is established, the Subject sends the requested credential(s) to the Verifier.  The Verifier checks the signature(s) on those credentials against DIDs of the Issuer(s) on the [blockchain](https://gitlab.com/caelum-labs/Lorena/lorena-substrate).

![Matrix Subject-Verifier Flow](../public/matrix-subject-verifier-flow.png "Matrix Subject-Verifier Flow")