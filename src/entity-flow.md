# DID and VC lifecycle

In the Lorena system the Verifiable Credentials (VCs) flow between the principal entities which are identified by Decentralized Identifiers (DIDs).

![Entity Sequence Diagram](../public/sequence-diagram.png "Entity Sequence Diagram")
