# Creating a DID

Each entity establishes a [Decentralized Identifier (DID)](https://w3c-ccg.github.io/did-primer/#introduction-0) by creating a private-public key pair and creating an identity containing that public key on a public blockchain, controlled by the private key. This DID resolves to a [DID Document](https://w3c-ccg.github.io/did-primer/#did-documents-0) which contains public keys, protocols, and service endpoints to interact with the entity.  Therefore, the DID is under the control of the entity, as it has the private key that allows the DID to point to a different DID Document.

[Lorena Substrate](https://gitlab.com/caelum-tech/Lorena/lorena-substrate) is a [Substrate](https://www.parity.io/substrate/)-based blockchain for publishing DIDs and their associated public keys.

[caelum-diddoc](https://gitlab.com/caelum-tech/caelum-diddoc) is a Rust library (with Javascript bindings) for the creation and manipulation of DID Documents.

![Creating a DID sequence diagram](../public/create-did.png "Creating a DID sequence diagram")
