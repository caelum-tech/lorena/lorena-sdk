# Lorena Identity Container

Decentralized Identities (DIDs) are simple unique identifiers in the format `did:example:123456789abcdefghi`.  A DID resolves to a DID Document (a JSON file) which contains information associated with the DID, such as ways to cryptographically authenticate the entity in control of the DID, services that can be used to interact with the entity, Verifiable Credentials associated with the DID.

An **Identity Container** is a secure way of storing DIDs, DID Documents, the private keys used to control them, Verifiable Credentials and Verifiable Presentations, and other assets used by an entity.  It is similar to a digital wallet (such as Trezor, Ledger, or MyEtherWallet) in that it stores private keys, but it also stores more complex documents which express a Decentralized Identity.

An identity container provides methods of accessing those documents and providing information about the data stored in those documents without sharing the actual information.  This is achieved by using [Zero-Knowledge Proofs](crypto.md#zkp).

## Implementations

The [Lorena Playground](https://gitlab.com/caelum-tech/lorena/lorena-playground) is the proof-of-concept identity container for the Lorena ecosystem.  It is deployed at [lorena.caelumlabs.com](https://lorena.caelumlabs.com).

<a name="pds"></a>The [Personal Data Store (PDS)](https://gitlab.com/caelum-tech/lorena/lorena/lorena-pds) is the first production-quality, full-service  identity container implemented for Lorena.  It is the reference implementation for a node in the Lorena ecosystem.
