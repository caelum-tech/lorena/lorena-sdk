# Requesting issuance of a VC

The Subject can create new credentials and receive Verified Credentials from Issuers.

In Lorena this is implemented in the [Lorena Identity Playground](https://gitlab.com/caelum-tech/Lorena/lorena-playground) (the **Subject**) and the [Lorena Matrix Daemon](https://gitlab.com/caelum-tech/Lorena/lorena-matrix-daemon) (the **Issuer**).

## Simplified flow
The Subject and Issuer each have a DID which can be verified with a public blockchain. The subject requests a credential and the issuer provides it.
![Requesting issuance of a VC sequence diagram](../public/subject-issuer-flow.png "Requesting issuance of a VC sequence diagram")

## Comprehensive flow
Since the Subject and Issuer do not necessarily have publicly addressable endpoints the components use Matrix to communicate with each other.  Because the Subject is a single-page application operating without a verified Matrix account, we use a Matrix Guest account (having limited capabilities to avoid spam), which joins an accessible public room in Matrix to request to request a credential.

Once the Issuer and Subject are in a private room together, they mutually verify their identities and agree on the terms of use (TOU) for the data that will be shared. This is in the form of a smart contract signed (and stored) by both parties.

After the secure channel is established, the Subject can send information to request the credential. The Issuer takes whatever steps it deems necessary to verify the Subject's information, then issues a signed verified credential which the Subject stores in its identity container.

![Matrix Subject-Issuer Flow](../public/matrix-subject-issuer-flow.png "Matrix Subject-Issuer Flow")
