# Cryptography

[Public-key cryptography](https://en.wikipedia.org/wiki/Public-key_cryptography) is fundamental to the Lorena solution. It is used nearly universally to secure the connections between phones, computers, and servers.  The most well-known use of public-key cryptography is the HTTPS visible in web browsers, which depends upon registries maintained by certificate authorities, and are used only to identify the companies which run the sites visited.  The use of public-key cryptography for individuals to identify themselves with [personal certificates](https://www.globalsign.com/en/personalsign/) tied to certificate authorities has been possible from the start, but has never reached widespread use (due the difficulty of managing and recovering private keys, and cost).

Many governments issue private keys to individuals, either in the form of chips in ID cards or in downloadable digital certificates.  These are generally useful for specific fiscal activities such as tax filing, but they are currently not useful for communication with anyone but the issuing authority.  Banks also issue private keys in the same way, but these are similarly not useful outside that same bank (or proprietary payment network).  Web browsers, chat clients and operating systems also generate private keys to secure end-to-end communication, but their scope is limited to the specific application (and sometimes to a single use).

Beyond the obvious benefits of keeping sensitive information secret, exchanging data privately, and verifying the sender of a message, cryptography powers another powerful tool: the Zero-Knowledge Proof.

## Zero-Knowledge Proofs
<a name="zkp"></a>

A [Zero-Knowledge Proof (ZKP)](https://en.wikipedia.org/wiki/Zero-knowledge_proof) allows a subject to demonstrate that they hold information without disclosing that information to the verifier.  In Lorena they are necessary in the following scenarios:

* Demonstrate that the subject has verified an email address with a specific domain (such as @gmail.com) without disclosing that email address to the verifier.
* Verify that a subject's legal name is on their passport without the verifier seeing the passport or any other data on it.
* Demonstrate that the subject has state-issued ID that shows that they are a legal adult without the verifier seeing it.
* Prove that an educational institution has issued a professional qualification to the subject, without viewing the subject's name, the name of the educational institution, the date granted, or the specific coursework.

In each of these cases, *the Verifier trusts the Issuer* of these credentials (not the Subject).  There is no intermediary between the Verifier and the Issuer.  The Issuer is the party that is responsible for verifying the evidence submitted about the Subject.  The Verifier is needs proof that the Subject (or Holder) has the correct type of credential signed by the Issuer.  This is achieved with a ZKP.  For more information, see [verifiable credentials](https://www.w3.org/TR/vc-data-model/#what-is-a-verifiable-credential) and [zero-knowledge proofs](https://www.w3.org/TR/vc-data-model/#zero-knowledge-proofs).

## Cryptographic libraries

Although most operating systems and programming languages have *fairly* complete cryptographic libraries, these libraries often do not support the types of encryption used in some blockchains, and when they do, they may vary according to version and platform.  They also do not as yet include any ZKP support, as that is very new technology.

Using a cryptographic library that is cross-platform allows us to work on more platforms easily, while providing better quality control and verification.  It also allows us to develop ZKP processes without low-level code.  However, this does come at a cost: each cryptographic library has its own advantages and disadvantages.  Although there will eventually be abstraction layers which function with multiple cryptographic providers, today we have to make a choice, and we have chosen Zenroom.

### Zenroom
<a name="zenroom"></a>

[Zenroom](https://zenroom.org) is a secured environment for cryptographic work.  It provides a virtual machine and scripting environment for public key encryption and ZKP.

Zenroom is released under the [Affero GNU Public License (AGPL)](https://en.wikipedia.org/wiki/Affero_General_Public_License), which means that *if modified*, its inclusion in software (even server-side) requires that that other software be made available under the same terms.  However, simply using the software server-side does not mean that all software on the server must be open source.  Lorena uses Zenroom but does not modify or extend its functionality.

As an example: MySQL and Amazon RDS.  MySQL is distributed under a GPL license.  Amazon built Amazon RDS based on MySQL, but they don't share the code they changed because they don't distribute the compiled binaries or object code: they simply sell the service.  If MySQL had been released under the AGPL, Amazon would have had to share the source code for the changes they made to MySQL.

### Other alternatives

ZKP is a fast-evolving field, and many commercial and open source solutions are being released constantly.

* [Aztec Protocol](https://www.aztecprotocol.com/)

* [Circom](https://github.com/iden3/circom) Circuit Compiler for zkSNARKs
