# Matrix

[Matrix](https://matrix.org/) is an open-source decentralized communication service used for secure communicate between the different entities and services.  It is the basis for the [Riot](https://riot.im/) messaging service.

Lorena uses Matrix as a transport mechanism for communication of Verifiable Credentials (VCs) and other information between different Entities in the system:

* [Getting a VC: Subject → Issuer communication](requesting-issuance-of-a-vc.md)
* [Using a VC: Subject → Verifier communication](requesting-and-verifying-a-vc.md)

Matrix is a decentralized service, but it is not peer-to-peer.  It consists of a network of federated [homeservers](https://matrix.org/faq/#what-is-a-homeserver%3F) which provide synchronization services with other homeservers and serve as a common endpoint for the clients which connect to it.

## Accounts and Homeservers
Each user communicating through Matrix must have an account.  There are two types of accounts, regular and guest.  Regular user accounts can participate fully in any activity, but depending on the configuration of the homeserver they may require manual human intervention: on the matrix.org homeserver they are protected by CAPTCHAs, email or phone number verification, acceptance of terms and conditions, and/or other roadblocks.

### Public Homeservers
Anonymous guest accounts have fewer capabilities because of the possibility of abuse, but they are much faster to create and do not require verification.  However, on matrix.org they do require acceptance of terms, which is automated in [lorena-matrix-helpers/`consentToMatrixTOS`](https://gitlab.com/caelum-tech/lorena/lorena-matrix-helpers/blob/master/src/matrixRooms.js#L14).

Establishment of a communication channel between two guest accounts must be mediated by a regular user who can set up a public room in which the two guest accounts can converse. This is implemented in [lorena-matrix-daemon](https://gitlab.com/caelum-tech/lorena/lorena-matrix-daemon/blob/test/index.js#L52).

### Private Homeservers
The advantage to setting up your own homeserver is that you can set the level of verification yourself.  Your homeserver can be federated into the larger Matrix network (or not) to allow interoperability with other organizations.