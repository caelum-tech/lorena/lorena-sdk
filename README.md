# Lorena SDK
Software Development Kit for the Lorena identity system

|License|Branch|Pipeline|Coverage|
|:-:|:-:|:-:|:-:|
|<a rel="license" href="http://creativecommons.org/licenses/by-sa/4.0/"><img alt="Creative Commons License" style="border-width:0" src="https://i.creativecommons.org/l/by-sa/4.0/80x15.png" /></a>|[`master`](https://gitlab.com/caelum-tech/Lorena/lorena-sdk/tree/master)|[![pipeline status](https://gitlab.com/caelum-tech/Lorena/lorena-sdk/badges/master/pipeline.svg)](https://gitlab.com/caelum-tech/Lorena/lorena-sdk/commits/master)|[![coverage report](https://gitlab.com/caelum-tech/Lorena/lorena-sdk/badges/master/coverage.svg)](https://gitlab.com/caelum-tech/Lorena/lorena-sdk/commits/master)|


## Introduction
**Lorena** provides Self-Sovereign Digital Identity, a method that lets people control how and where their personal information is used while accessing services online. [Read more.](src/intro.md)

## Build documentation

### Tools
* [PlantUML](http://plantuml.com)
`sudo apt install plantuml`

### Build Process
`./build`

## Related repositories
* [Lorena Identity Playground](https://gitlab.com/caelum-tech/Lorena/lorena-playground)
* [Demo website: Hoverfloat](https://gitlab.com/caelum-tech/Lorena/lorena-demo-hoverfloat)
* [Lorena Substrate](https://gitlab.com/caelum-tech/Lorena/lorena-substrate)
* [Lorena Matrix Client](https://gitlab.com/caelum-tech/Lorena/lorena-matrix-client)
* [Lorena Matrix Daemon](https://gitlab.com/caelum-tech/Lorena/lorena-matrix-daemon)
* [Lorena Matrix Helpers](https://gitlab.com/caelum-tech/Lorena/lorena-matrix-helpers)
* [caelum-diddoc](https://gitlab.com/caelum-tech/caelum-diddoc)
* [caelum-vcdm](https://gitlab.com/caelum-tech/caelum-vcdm)
